#include "serveur_centralise.h"



void libere_ressources_processus_fils(int signal)
{
	int temp_errno = errno;

	while(waitpid(-1,NULL,WNOHANG) > 0);

	errno = temp_errno;

}



void affiche_erreur(int erreur, char *fonction)
{

	int err = erreur;
	
	system("clear");
	fprintf(stderr," Fonction : %s \n Erreur : %s \n\n",fonction,strerror(err));

	exit(EXIT_FAILURE);

}

int creer_nouveau_compte(char *nom , char *mdp)
{

	FILE *fichier_login = NULL;
	char nom2[NAME_SIZE] = {'\0'};
	int est_nom_existant = 0;
	int decalage = 1;

	if((fichier_login = fopen("ID_utilisateurs/identifiant_utilisateur.txt","r+")) == NULL)
	{
		fprintf(stderr,"\n\n\n Une erreur est survenue lors de l'ouverture du fichier contenant les identifiants : %s \n\n\n",strerror(errno));
		return 0;
	}
	else
	{
		if(fseek(fichier_login,35,SEEK_SET) != 0)
		{
			fclose(fichier_login);
			fprintf(stderr,"\n\n\n Une erreur est survenue lors du traitement du fichier contenant les identifiants : %s \n\n\n",strerror(errno));
			return 0;
		}
		
		while(!est_nom_existant && fgets(nom2,NAME_SIZE,fichier_login) != NULL)
		{
			if(!strncmp(nom,nom2, strlen(nom)))
				est_nom_existant = 1;
			
			if(decalage == 3)
			{
				decalage = 0;

				if(fseek(fichier_login,1,SEEK_CUR) != 0)
				{
					fclose(fichier_login);
					fprintf(stderr,"\n\n\n Une erreur est survenue lors du traitement du fichier contenant les identifiants : %s \n\n\n",strerror(errno));
					return 0;
				}
			}

			decalage += 1;
			memset(nom2,'\0',(size_t) NAME_SIZE);
		}

		if(est_nom_existant)
		{
			
			fclose(fichier_login);
			fprintf(stderr,"\n\n\n Impossible de créer un nouvel utilisateur car %s existe déjà \n\n\n",nom);
			return 0;

		}
		else
		{
			fprintf(fichier_login,"%s",nom);

			for(int i=0; i<NAME_SIZE; i++)
				fputc(' ',fichier_login);           
							
			fprintf(fichier_login,"%s\n",mdp);

	
			fprintf(stdout,"\n\n\n %s a été ajouté à la liste des utilisateurs \n\n\n",nom);
			fclose(fichier_login);
			return 1;

		}


	}

}


int authentifier_utilisateur(char *nom , char *mdp)
{
	
	FILE *fichier_login = NULL;
	char nom2[NAME_SIZE] = {'\0'};
	int est_nom_existant = 0;
	int est_mdp_correct = 1;
	int decalage = 1;

	if((fichier_login = fopen("ID_utilisateurs/identifiant_utilisateur.txt","r")) == NULL)
	{
		fprintf(stderr,"\n\n\n Une erreur est survenue lors de l'ouverture du fichier contenant les identifiants : %s \n\n\n",strerror(errno));
		return 0;
	}
	else
	{
		if(fseek(fichier_login,35,SEEK_SET) != 0)
		{
			fclose(fichier_login);
			fprintf(stderr,"\n\n\n Une erreur est survenue lors du traitement du fichier contenant les identifiants : %s \n\n\n",strerror(errno));
			return 0;
		}
		
		while(!est_nom_existant && fgets(nom2,NAME_SIZE,fichier_login) != NULL)
		{	if(!strncmp(nom,nom2, strlen(nom)))
				est_nom_existant = 1;
			
			if(decalage == 3)
			{
				decalage = 0;

				if(fseek(fichier_login,1,SEEK_CUR) != 0)
				{
					fclose(fichier_login);
					fprintf(stderr,"\n\n\n Une erreur est survenue lors du traitement du fichier contenant les identifiants : %s \n\n\n",strerror(errno));
					return 0;
				}
			}

			decalage += 1;
			memset(nom2,'\0',(size_t) NAME_SIZE);
		}

		if(est_nom_existant)
		{
			int c;
			
			while((c = fgetc(fichier_login)) == ' ');
			
			if(fseek(fichier_login,-1,SEEK_CUR) != 0)
			{
				fclose(fichier_login);
				fprintf(stderr,"\n\n\n Une erreur est survenue lors du traitement du fichier contenant les identifiants : %s \n\n\n",strerror(errno));
				return 0;
			}
		
			for(int i=0; i< strlen(mdp); i++)
			{
				c = fgetc(fichier_login);
				
				if(mdp[i] != c)
				{
					est_mdp_correct = 0;
					break;
					
				}
			}	

			fclose(fichier_login);
			
			if(est_mdp_correct)
			{	
				fprintf(stderr,"\n\n\n Authentification de  %s réussi \n\n\n",nom);
				return 1;
			}
			else
			{
				fprintf(stderr,"\n\n\n Le mot de passe associé à %s est incorrect \n\n\n",nom);
				return 0;
			}	

		}
		else
		{

			fprintf(stdout,"\n\n\n %s n'existe pas \n\n\n",nom);
			fclose(fichier_login);
			return 0;

		}


	}

}


int rechercher_fichier(char *nom_fichier, char *liste_hote, int *compteur)
{
	
	FILE *desc_fichier = NULL;
	DIR *repertoire_desc_fichiers_partages = NULL;
	struct dirent *liste_des_fichiers = NULL;
	char chemin_complet[1024];
	char fichier[256];
	int offset = 0;
	*compteur = 0;
	int c = 0;
	

	if(!(repertoire_desc_fichiers_partages = opendir("./Description_fichiers_partages")))
	{
		fprintf(stderr,"Le dossier : Description_fichiers_partages est inacessible \n\n");
		return 0;
	}		
	if(!(liste_des_fichiers = readdir(repertoire_desc_fichiers_partages)))
	{
		fprintf(stderr,"La lecture du dossier : Description_fichiers_partages a échoué \n\n");
		closedir(repertoire_desc_fichiers_partages);
		return 0;
	}
										
	while(liste_des_fichiers != NULL)
	{
		memset(chemin_complet,'\0',(size_t)1024);
		memset(fichier,'\0',(size_t)256);
		
		if((liste_des_fichiers->d_type == DT_REG) && strncmp(".",liste_des_fichiers->d_name,(size_t)1) && strncmp("..",liste_des_fichiers->d_name,(size_t)2))
		{
			snprintf(chemin_complet,(size_t)1024,"./Description_fichiers_partages/%s",liste_des_fichiers->d_name);
			
			if(!(desc_fichier = fopen(chemin_complet,"r")))
			{
				fprintf(stderr,"\n\n\n Une erreur est survenue lors de l'ouverture du fichier %s : \n %s \n\n\n",chemin_complet,strerror(errno));
				closedir(repertoire_desc_fichiers_partages);
				return 0;
			}
			else
			{
				while((c = fscanf(desc_fichier,"%s\n",fichier)) != EOF)
				{
					if(!strncmp(nom_fichier,fichier,strlen(nom_fichier)))
					{
						snprintf(liste_hote + offset,strlen(liste_des_fichiers->d_name) + 3,"%s   ",liste_des_fichiers->d_name);
						
						offset += (int) strlen(liste_des_fichiers->d_name) + 3;
						
					}
					
					memset(fichier,'\0',(size_t)256);
						
				}
				
				fclose(desc_fichier);
			}
			
			*compteur += 1;		
		}
		
		liste_des_fichiers = readdir(repertoire_desc_fichiers_partages);
		
	}
		
	if(*compteur == 0)
	{
		fprintf(stderr," Le dossier : Description_fichiers_partages ne contient aucun fichier \n\n");
		closedir(repertoire_desc_fichiers_partages);
		return 0;
	}
	if(closedir(repertoire_desc_fichiers_partages) != 0)
		return 0;

	return 1;
}

void traite_requete_client(int accept_sockfd,struct sockaddr_in *addr_client)
{
	FILE *F = NULL;
	int taille_fichier_desc = 0;
	int rv = 0;
	int quitter = 0;
	char fichier[DATA_SIZE];
	char nom[NAME_SIZE], mdp[PASSWD_SIZE];
	char requete[LONGUEUR_CODE];
	char reponse[LONGUEUR_CODE];
	char message[BUFFER_SIZE];
	char addr_ip_client[INET_ADDRSTRLEN];
	char *buffer = NULL;
	int offset = 0;
	int compteur = 0;
	char liste_hote[5000] = {'\0'};
	int c = 0;

	inet_ntop(addr_client->sin_family,(const struct sockaddr *)&addr_client->sin_addr,addr_ip_client,sizeof *addr_client);
	printf(" %s ----------- essaye de se connecter \n\n",addr_ip_client);

	while(!quitter)
	{
		memset(requete,'\0', (size_t) LONGUEUR_CODE);
		memset(message,'\0',(size_t) BUFFER_SIZE);
		memset(reponse,'\0',(size_t) LONGUEUR_CODE);
		memset(nom,'\0',(size_t) NAME_SIZE);
		memset(mdp,'\0',(size_t) PASSWD_SIZE);
		memset(fichier,'\0',(size_t) DATA_SIZE);

		if((rv = recv(accept_sockfd, message,BUFFER_SIZE, 0)) > 0)
		{
			snprintf(requete,(size_t) LONGUEUR_CODE,"%s", message);
				
			if(!strncmp(SIGN_UP,requete , strlen(SIGN_UP)) || !strncmp(LOG_IN,requete , strlen(LOG_IN)))
            {
				snprintf(nom,(size_t)NAME_SIZE,"%s", message+LONGUEUR_CODE);
				snprintf(mdp,(size_t)PASSWD_SIZE,"%s", message+LONGUEUR_CODE+NAME_SIZE);
			
				if(!strncmp(SIGN_UP,requete , strlen(SIGN_UP)))
				{
					fprintf(stdout, " %s -----> création de compte : %s\n\n",addr_ip_client,requete);
					rv = creer_nouveau_compte(nom,mdp);
				}	
				else
				{
					fprintf(stdout, " %s -----> connexion : %s\n\n",addr_ip_client,requete);
					rv = authentifier_utilisateur(nom ,mdp);
				}	

				if(rv)
                {
					if(send(accept_sockfd,SIGN_UP_OK,strlen(SIGN_UP_OK), 0) > 0)
                	{
						memset(requete,0,(size_t) LONGUEUR_CODE);

						if(recv(accept_sockfd,requete,(size_t) LONGUEUR_CODE, 0) <= 0)		
						{	
							close(accept_sockfd);
							return;
						}	
						if(!strncmp(SND_DESC,requete ,strlen(SND_DESC)))
						{
							fprintf(stdout, " %s -----> Envoi de la description de fichiers : %s\n\n",addr_ip_client,requete);

							if(send(accept_sockfd,SND_DESC_OK,strlen(SND_DESC_OK), 0) > 0)
							{
								memset(requete,0,(size_t)LONGUEUR_CODE);
								
								if((rv = (int)recv(accept_sockfd,requete,(size_t)LONGUEUR_CODE, 0)) <= 0)
								{
									fprintf(stdout, " %s -----> la réception de la taille du fichier a échoué (rv = %d)\n\n",addr_ip_client,rv);
									close(accept_sockfd);
									return;
								}
								
								taille_fichier_desc = atoi(requete);
								
								if(taille_fichier_desc > 0)
								{
									if(!(buffer = calloc((size_t)taille_fichier_desc,sizeof(char))))
									{
										fprintf(stdout, " %s -----> Allocation mémoire échouée \n\n",addr_ip_client);
										close(accept_sockfd);
										return;
									}	
									if(send(accept_sockfd,OKI,strlen(OKI), 0) <= 0)
									{
										free(buffer);
										close(accept_sockfd);
										return;
									}
									
									fprintf(stdout, " %s -----> réception du fichier en cours\n\n",addr_ip_client);
									
									while(offset < (int)taille_fichier_desc)
									{
										rv = (int) recv(accept_sockfd,buffer+offset,(size_t)(taille_fichier_desc - offset), 0);
										
										if(rv < 0)
										{
											fprintf(stdout, " %s -----> la réception du fichier a échoué (rv) \n\n",addr_ip_client);
											free(buffer);
											close(accept_sockfd);
											return;
										}
										
										offset += rv;
									}
									
									memset(message,0,(size_t)BUFFER_SIZE);
									snprintf(message,(size_t)DATA_SIZE,"./Description_fichiers_partages/%s",nom);
									
									if(!(F = fopen(message,"w+")))
									{
										fprintf(stdout, " %s -----> la réception du fichier a échoué (fopen)\n\n",addr_ip_client);
										free(buffer);
										close(accept_sockfd);
										return;
									}
									
									fprintf(F,"%s",buffer);
									fclose(F);
									
									
									if(!(F = fopen("fichier_DNS.txt","a+")))
									{
										fprintf(stdout, " La copie des informations dans le fichier de résolution d'adresse en nom a échouée \n\n");
										free(buffer);
										close(accept_sockfd);
										return;
									}
									
									int tmp = (int)ntohs(addr_client->sin_port);
									fprintf(F,"%s-%s-%d\n",nom,addr_ip_client,tmp+1);
									fclose(F);
									
									free(buffer);
									close(accept_sockfd),quitter = 1;
									fprintf(stdout, " %s -----> Fichier bien reçu \n\n",addr_ip_client);
							}
							else
							{	
								fprintf(stdout, " %s -----> n'a aucun fichier à partager \n\n",nom);
								
								if(send(accept_sockfd,OKI,strlen(OKI), 0) <= 0)
								{
									free(buffer);
									close(accept_sockfd);
									return;
								}
								close(accept_sockfd);
								quitter = 1;
							}	

						}
						else
						{

							if(send(accept_sockfd,SND_DESC_NOT_OK,strlen(SND_DESC_NOT_OK), 0) > 0)
							{
								close(accept_sockfd),quitter = 1;
							}
							else
							{	
								close(accept_sockfd);
								return;
							}	

						}		
                	}
                	else
					{
						close(accept_sockfd);
						return;
					}
                }	
 				else
 				{	
                	if(send(accept_sockfd,SIGN_UP_NOT_OK,strlen(SIGN_UP_NOT_OK), 0) <= 0)
                	{
                    	close(accept_sockfd);
                    	return;
                	}
                	else
						close(accept_sockfd),quitter = 1;
				}
 
             }
             else
			 {	
                if(send(accept_sockfd,SIGN_UP_NOT_OK,strlen(SIGN_UP_NOT_OK), 0) <= 0)
                {
                    close(accept_sockfd);
                    return;
                }
                else
					close(accept_sockfd),quitter = 1;
			}
 
          }
          else if(!strncmp(FILE_LOOKUP,requete , strlen(FILE_LOOKUP)))
          {
				snprintf(fichier,(size_t)DATA_SIZE,"%s", message+LONGUEUR_CODE);
			
				fprintf(stdout, " %s -----> recherche de fichiers distants : %s\n\n",addr_ip_client,requete);
				rv = rechercher_fichier(fichier,liste_hote, &compteur);
				
				if(rv == 0)
				{	
					close(accept_sockfd);
					quitter = 1;
				}
									
				
				else
                {
					if(send(accept_sockfd,liste_hote,strlen(liste_hote), 0) <= 0)
						close(accept_sockfd),quitter = 1;
					else	
					{
						memset(fichier,'\0',(size_t) DATA_SIZE);
						
						if(recv(accept_sockfd,fichier,(size_t)DATA_SIZE, 0) <= 0)		
						{	
							close(accept_sockfd);
							return;
						}	
						else
						{
							memset(message,0,(size_t)BUFFER_SIZE);
							
							if(!(F = fopen("fichier_DNS.txt","r")))
							{
								fprintf(stdout, " La recherche de l'hôte distant n'a pas abouti \n Vérifiez qu'il s'agit du bon hôte \n\n");
								close(accept_sockfd);
								return;
							}
									
							while((c = fscanf(F,"%s\n",message)) != EOF)
							{
								if(!strncmp(fichier,message,strlen(fichier)))
								{
									compteur = 255;
									break;
								}	
								
								memset(message,0,(size_t)BUFFER_SIZE);
									
							}
							
							fclose(F);		
									
							if(compteur != 255)
							{
								fprintf(stdout, " La recherche de l'hôte distant n'a pas abouti \n Vérifiez qu'il s'agit du bon hôte \n\n");
								close(accept_sockfd);
								return;
							}
							if(send(accept_sockfd,message,strlen(message), 0) <= 0)
								close(accept_sockfd),quitter = 1;
							else	
								close(accept_sockfd),quitter = 1;
							
						}
					}	

 
				}
 
			}
		}
		else
			fprintf(stderr,"\n Connexion échouée : rv = %d\n\n",rv),quitter = 1, close(accept_sockfd);

	}



}



int main()
{

	int listen_sockfd = -1 , accept_sockfd;
	int optval = 1;
	struct sockaddr_in addr_serveur;
	struct sockaddr_in addr_client;
	socklen_t addr_client_size = sizeof addr_client;
	struct sigaction sa;
	pid_t processus_fils;


	memset(&addr_serveur,0, sizeof addr_serveur);
	memset(&addr_client,0, sizeof addr_client);

	addr_serveur.sin_family = AF_INET;
	addr_serveur.sin_port = htons(PORT);
	//inet_pton(addr_serveur.sin_family, "127.0.0.1", &(addr_serveur.sin_addr.s_addr));


	if((listen_sockfd = socket(AF_INET,SOCK_STREAM, 0)) == -1)
		affiche_erreur(errno, "socket()");

	if(setsockopt(listen_sockfd,SOL_SOCKET,SO_REUSEADDR,&optval,sizeof(int)) == -1)
    	affiche_erreur(errno, "setsockopt()");

	if(bind(listen_sockfd,(const struct sockaddr *) &addr_serveur, (socklen_t) sizeof addr_serveur) < 0)
		close(listen_sockfd) ,affiche_erreur(errno, "bind()");


	sa.sa_handler = libere_ressources_processus_fils;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;

	if(sigaction(SIGCHLD,&sa,NULL) == -1 )
    	close(listen_sockfd),affiche_erreur(errno,"sigaction()");

	if(listen(listen_sockfd, NB_MAX_CONNECTION) == -1)
		close(listen_sockfd) , affiche_erreur(errno,"listen() ");

	fprintf(stdout, " Serveur : En attente de connexion ....... \n\n");

	while(!quitter_serveur)
	{

		accept_sockfd = accept(listen_sockfd,(struct sockaddr *) &addr_client, &addr_client_size);

		if(accept_sockfd != -1)
        {
		processus_fils = fork();

			if(processus_fils == -1)
				close(listen_sockfd),close(accept_sockfd) , affiche_erreur(errno, "fork()");
			else if(processus_fils == 0)
				break;
			else
				close(accept_sockfd);
				
		}
		else
	   		close(listen_sockfd), affiche_erreur(errno,"accept()");

	}

	close(listen_sockfd);


	if(!processus_fils)
		traite_requete_client(accept_sockfd,&addr_client);

	return 0;

}
