#ifndef SERVEUR_CENTRALISE
#define SERVEUR_CENTRALISE


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/stat.h>
#include <dirent.h>


#define PORT 50000
#define NB_MAX_CONNECTION 15
#define NAME_SIZE 13
#define PASSWD_SIZE 13
#define ENCRYPTED_PASSWD_SIZE 13
#define DATA_SIZE 64
#define BUFFER_SIZE 256
#define LONGUEUR_CODE 20


#define SIGN_UP_OK "SIGN_UP_OK"
#define SIGN_UP_NOT_OK "SIGN_UP_NOT_OK"
#define LOG_IN_OK "LOG_IN_OK"
#define LOG_IN_NOT_OK "LOG_IN_NOT_OK"
#define SND_DESC_OK "SND_DESC_OK"
#define SND_DESC_NOT_OK "SND_DESC_NOT_OK"
#define OKI "OKI"
#define NOT_OK "NOT_OK"


#define SIGN_UP "SIGN_UP"
#define LOG_IN  "LOG_IN"
#define SND_DESC "SND_DESC"
#define FILE_LOOKUP "FILE_LOOKUP"



typedef enum 
{
	DEMANDE_CONN	


}code_requete;


int quitter_serveur = 0;

void libere_ressources_processus_fils(int signal);
void quitter_programme_serveur(int signal);
void affiche_erreur(int erreur, char *fonction);
int creer_nouveau_compte(char *nom , char *mdp);
int authentifier_utilisateur(char *nom , char *mdp);
void traite_requete_client(int accept_sockfd,struct sockaddr_in *addr_client);


#endif
