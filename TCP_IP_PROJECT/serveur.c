#include "serveur.h"


/*
 *  
 *
 *      Fonctions gérant la partie serveur d'un hôte.
 *
 *
*/




char *chiffrer_mdp(char *mdp)
{
	return crypt((const char *) mdp, "I3");

}

void init_serveur()
{
	int fd,sockfd;
	int status_conn_ok = 0;
	char message[TAILLE_MESSAGE_CONNEXION] = {'\0'}; 
	char adresse_serv_central[BUFFER_SIZE] = {'\0'};
	char port_serv_central[BUFFER_SIZE] = {'\0'};
	char requete[LONGUEUR_CODE] = {'\0'};
	char nom[NAME_SIZE] = {'\0'};
	char mdp[PASSWD_SIZE] = {'\0'};
	struct sockaddr_in addr;
	struct sockaddr_in client;
	INFO_SERVEUR serveur;
	char temp[4] = {'\0'};
	char fichier[5000] = {'\0'};
	int quitter_serveur = 0;
	socklen_t addr_client_size = sizeof client;
	char nom_fichier[DATA_SIZE] = {'\0'};
	struct stat statbuf;
	
	
	//pid_t fils;
	uint16_t ancien_port = 44444;
	char liste_utilisateur[5000] = {'\0'};
	int c;
	
	struct sockaddr_in addr_tmp;
	socklen_t addrlen = (socklen_t) sizeof(addr_tmp);
	memset(&(addr_tmp),0,sizeof addr_tmp);

	memset(&serveur,0,sizeof serveur);
	memset(&(serveur.addr_serveur),0,sizeof serveur.addr_serveur);
	serveur.addr_serveur.sin_family = AF_INET;
	serveur.addr_serveur.sin_port = htons(0);
	
	
	snprintf(liste_utilisateur,(size_t)3000,"echo 'OK 1' > toto"),(c = system(liste_utilisateur));
	
	if((fd = open("/tmp/tube",O_RDWR)) == -1)
		fprintf(stderr,"\n L'ouverture du tube nommé a échoué (serveur) : %s \n\n",strerror(errno)) , exit(EXIT_FAILURE);
		
	
	//int optval = 1;
	
	while(!status_conn_ok)
	{
		memset(message,'\0',(size_t) TAILLE_MESSAGE_CONNEXION);
		memset(requete,'\0',(size_t) LONGUEUR_CODE);
		memset(adresse_serv_central,'\0',(size_t) BUFFER_SIZE);
		memset(port_serv_central,'\0',(size_t) BUFFER_SIZE);
		memset(nom,'\0',(size_t) NAME_SIZE);
		memset(mdp,'\0',(size_t) PASSWD_SIZE);
		
		if(read(fd,message,(size_t) TAILLE_MESSAGE_CONNEXION) < 0)
			fprintf(stderr,"\n La lecture du tube nommé a échoué : %s \n\n",strerror(errno)) , exit(EXIT_FAILURE);	
		
		snprintf(requete, (size_t) LONGUEUR_CODE - 1, "%s",message);
		snprintf(adresse_serv_central,(size_t) BUFFER_SIZE - 1,"%s",message + LONGUEUR_CODE);
		snprintf(port_serv_central, (size_t) BUFFER_SIZE - 1,"%s", message + LONGUEUR_CODE + BUFFER_SIZE);
		snprintf(nom, (size_t) NAME_SIZE - 1, "%s",message + LONGUEUR_CODE + BUFFER_SIZE + BUFFER_SIZE);
		snprintf(mdp, (size_t) PASSWD_SIZE - 1, "%s",message + LONGUEUR_CODE + BUFFER_SIZE + BUFFER_SIZE + NAME_SIZE);
		
		snprintf(liste_utilisateur,(size_t)3000,"echo 'OK 2' >> toto"),(c = system(liste_utilisateur));
		
		if((serveur.sockfd = socket(AF_INET,SOCK_STREAM, 0)) < 0)
			status_conn_ok = 0,snprintf(liste_utilisateur,(size_t)3000,"echo 'socket a échoué %s ' >> toto",strerror(errno)),(c = system(liste_utilisateur));
		/*if(setsockopt(serveur.sockfd,SOL_SOCKET,SO_REUSEADDR,&optval,sizeof(int)) == -1)
		{
			close(serveur.sockfd);
			status_conn_ok = 0;
			snprintf(liste_utilisateur,(size_t)3000,"echo 'setsockopt a échoué %s ' > toto",strerror(errno)),(c = system(liste_utilisateur));
		}	*/
		else if(bind(serveur.sockfd, (const struct sockaddr *) &(serveur.addr_serveur),(socklen_t) sizeof serveur.addr_serveur) < 0 || getsockname(serveur.sockfd, (struct sockaddr *)&addr_tmp,&addrlen) < 0)
		{
			close(serveur.sockfd);
			status_conn_ok = 0;
			snprintf(liste_utilisateur,(size_t)3000,"echo 'bind a échoué %s ' > toto",strerror(errno)),(c = system(liste_utilisateur));
		}	
		else
			status_conn_ok = connexion_serveur_centralise(adresse_serv_central, port_serv_central, requete, nom , mdp , &addr,&serveur);
		
		snprintf(temp, (size_t)4, "%d",status_conn_ok);
		
		if(write(fd,temp,sizeof(*temp)) < 0)
			fprintf(stderr,"\n L'écriture dans le tube nommé a échoué : %s \n\n",strerror(errno)) , exit(EXIT_FAILURE);
	}
	
	//(int) ntohs(serveur.addr_serveur.sin_port)
	close(fd);
	close(serveur.sockfd);
	
	//snprintf(liste_utilisateur,(size_t)3000,"echo 'ancien port = %d ' > toto",(int) ancien_port);
	//c = system(liste_utilisateur);
	
	memset(liste_utilisateur,0,(size_t) 5000);
	
	ancien_port = ((int) ntohs(addr_tmp.sin_port)) + 1;
	memset(&(addr_tmp),0,sizeof addr_tmp);
	
	addr_tmp.sin_family = AF_INET;
	addr_tmp.sin_port = htons((uint16_t)ancien_port);
	
	if((serveur.sockfd = socket(AF_INET,SOCK_STREAM, 0)) < 0)
		status_conn_ok = 0,snprintf(liste_utilisateur,(size_t)3000,"echo 'socket2 a échoué %s ' >> toto",strerror(errno)),(c = system(liste_utilisateur)),exit(1);
	
		
	while(bind(serveur.sockfd, (const struct sockaddr *) &(addr_tmp),(socklen_t) sizeof addr_tmp) < 0 && errno ==  EADDRINUSE)
	{
		close(serveur.sockfd);
		serveur.sockfd = socket(AF_INET,SOCK_STREAM, 0);
		snprintf(liste_utilisateur,(size_t)3000,"echo 'bind2 a échoué %s !!! Trying again' > toto",strerror(errno)),(c = system(liste_utilisateur));
		
	}
	
	memset(liste_utilisateur,0,(size_t) 5000);
	snprintf(liste_utilisateur,(size_t)3000,"echo 'OK bind réussi ' > toto"),(c = system(liste_utilisateur));
	
	if(listen(serveur.sockfd,1) == -1)
		status_conn_ok = 0,snprintf(liste_utilisateur,(size_t)3000,"echo 'listen a échoué %s ' >> toto",strerror(errno)),(c = system(liste_utilisateur)),exit(1);
	
	snprintf(liste_utilisateur,(size_t)3000,"echo 'OK test réussi' >> toto"),(c = system(liste_utilisateur));
		
	while(!quitter_serveur)
	{			
		sockfd = accept(serveur.sockfd,(struct sockaddr *) &client, &addr_client_size);

		if(sockfd != -1)
        {
			//fils = fork();
			
			//if(fils == 0)
			//{
			
				memset(liste_utilisateur,0,(size_t) 5000);
				snprintf(liste_utilisateur,(size_t)3000,"echo 'En attente d'une requête de fichier: port d'écoute %d' >> toto",ancien_port);
				c = system(liste_utilisateur);
				
				if(recv(sockfd,fichier,(size_t)4000, 0) <= 0)
					close(sockfd);
				
				memset(requete,'\0',(size_t) LONGUEUR_CODE);
				snprintf(requete, (size_t) LONGUEUR_CODE, "%s",fichier);
				
				memset(nom_fichier,'\0',(size_t)DATA_SIZE);
				snprintf(nom_fichier, (size_t) DATA_SIZE, "%s",fichier + LONGUEUR_CODE);
				
				memset(fichier,'\0',(size_t)5000);
				
				memset(liste_utilisateur,0,(size_t) 5000);
				snprintf(liste_utilisateur,(size_t)3000,"echo 'fichier %s demandé' >> toto",nom_fichier);
				c = system(liste_utilisateur);
				
				if(strncmp(GET_FILE,requete , strlen(GET_FILE)))
				{	
					close(sockfd);
					return;
				}	
					
				snprintf(fichier,(size_t)1024,"./fichiers_partages/%s",nom_fichier);
							
				if(stat(fichier, &statbuf) == -1)
				{	
					close(sockfd);
					return;
				}
										
				memset(requete,0,(size_t) LONGUEUR_CODE);
				snprintf(requete,(size_t) LONGUEUR_CODE,"%d",(int)statbuf.st_size);
										
				if(send(sockfd,requete,(size_t) LONGUEUR_CODE, 0) < 0)
				{	
					close(sockfd);
					continue;
				}
				
				memset(liste_utilisateur,0,(size_t) 5000);
				snprintf(liste_utilisateur,(size_t)3000,"echo 'envoi  %s taille : %d ' >> toto",nom_fichier,(int)statbuf.st_size);
				c = system(liste_utilisateur);						
				memset(requete,0,(size_t) LONGUEUR_CODE);
										
				if(recv(sockfd, requete, LONGUEUR_CODE, 0) <= 0)
				{	
					close(sockfd);
					continue;
				}
				if(strncmp(OKI,requete, strlen(OKI)))
				{	
					close(sockfd);
					continue;
				}
				
				if(!statbuf.st_size)
				{	
					close(sockfd);
					continue;
				}
				
				if((fd = open(fichier, O_RDONLY)) == -1)
				{	
					close(sockfd);
					continue;
				}
		
				while((int) statbuf.st_size > 0)
				{
					if(sendfile(sockfd,fd,NULL, (size_t)1) < 0)
					{
						close(sockfd);
						return;
					}
					
					statbuf.st_size -= 1;
				}
														
				close(sockfd);		
				
			//}		
		}
		else
	   		close(serveur.sockfd) ,snprintf(liste_utilisateur,(size_t)3000,"echo 'accept a échoué %s ' > toto",strerror(errno)),(c = system(liste_utilisateur)),exit(1);

	}
	
	
	
}

int connexion_serveur_centralise(char *adresse_serv_central, char *port_serv_central, char *requete, char *nom , char *mdp, struct sockaddr_in *addr , INFO_SERVEUR *serveur)
{
	FILE *desc_fichier = NULL;
	int fd;
	struct stat statbuf;
	DIR *repertoire_fichiers_partages = NULL;
	struct dirent *liste_des_fichiers = NULL;

	char message[DATA_SIZE] = {'\0'};
	char reponse[LONGUEUR_CODE] = {'\0'};
	char *mdp_chiffre = chiffrer_mdp(mdp);
	int c;
		
	memset(&statbuf,0,sizeof(statbuf));
	
	memset(addr,0,sizeof(struct sockaddr_in));
	addr->sin_family = AF_INET;
	addr->sin_port = htons((uint16_t) atoi(port_serv_central));
	inet_pton(addr->sin_family,adresse_serv_central,&((addr->sin_addr).s_addr));

	/*memset(&(serveur->addr_serveur),0,sizeof serveur->addr_serveur);
	serveur->addr_serveur.sin_family = AF_INET;
	serveur->addr_serveur.sin_port = htons(45000);*/
	
	//inet_pton(serveur->addr_serveur.sin_family,"0.0.0.0",&(serveur->addr_serveur.sin_addr.s_addr));


	if(snprintf(message,(size_t) LONGUEUR_CODE,"%s",requete) < 0)
		return 0;
	
	if(snprintf(message + LONGUEUR_CODE,(size_t) NAME_SIZE,"%s",nom) < 0)
		return 0;
	
	if(mdp_chiffre != NULL && snprintf(message + LONGUEUR_CODE + NAME_SIZE,strlen(mdp_chiffre),"%s",mdp_chiffre) < 0)
		return 0;


	//else
	//{

	
		//else
		//{
			//*ancien_port = ntohs(serveur->addr_serveur.sin_port);
			
			if(connect(serveur->sockfd,(const struct sockaddr *) addr,(socklen_t) sizeof *addr) == -1)
			{
				c = system("echo 'connect a échoué' >> toto");
				close(serveur->sockfd);
				return 0;
			}
			else
			{

				if(!strncmp(SIGN_UP,requete , strlen(SIGN_UP)) || !strncmp(LOG_IN,requete , strlen(LOG_IN)))
				{
					if(send(serveur->sockfd,message,(size_t) DATA_SIZE, 0) <= 0)
					{
						close(serveur->sockfd);
						return 0;
					}

					if( recv(serveur->sockfd, reponse, LONGUEUR_CODE, 0) <= 0)
					{
						close(serveur->sockfd);
						return 0;
					}
					else
					{
						if(!strncmp(SIGN_UP_OK,reponse, strlen(SIGN_UP_OK)) || !strncmp(LOG_IN_OK,reponse, strlen(LOG_IN_OK)))
					    {
							
							if(send(serveur->sockfd,SND_DESC,(size_t) LONGUEUR_CODE, 0) <= 0)
							{
								close(serveur->sockfd);
								return 0;
							}

							memset(reponse,0,(size_t) LONGUEUR_CODE);
							
							if( recv(serveur->sockfd, reponse, LONGUEUR_CODE, 0) <= 0)
							{
								close(serveur->sockfd);
								return 0;
							}
							else
							{
								if(!strncmp(SND_DESC_OK,reponse, strlen(SND_DESC_OK)))
								{
									if(!(desc_fichier = fopen("./fichiers_partages/description_fichiers.txt","w+")))
									{
										close(serveur->sockfd);
										return 0;
									}
									
									if(!(repertoire_fichiers_partages = opendir("./fichiers_partages")))
									{
										close(serveur->sockfd);
										return 0;
									}
									if(!(liste_des_fichiers = readdir(repertoire_fichiers_partages)))
									{
										closedir(repertoire_fichiers_partages);
										close(serveur->sockfd);
										return 0;
									}
																		
									while(liste_des_fichiers != NULL)
									{
										if((liste_des_fichiers->d_type == DT_REG) && strncmp(".",liste_des_fichiers->d_name,(size_t)1) && strncmp("..",liste_des_fichiers->d_name,(size_t)2) && strncmp("description_fichiers.txt",liste_des_fichiers->d_name,(size_t)24))
											if(!fprintf(desc_fichier,"%s\n",liste_des_fichiers->d_name))
											{
												close(serveur->sockfd);
												return 0;
											}
											
										liste_des_fichiers = readdir(repertoire_fichiers_partages);
										
									}
										
									fclose(desc_fichier);
									
									if(closedir(repertoire_fichiers_partages) != 0)
									{
										close(serveur->sockfd);
										return 0;
									}
																	
									if(stat("./fichiers_partages/description_fichiers.txt", &statbuf) == -1)
									{
										close(serveur->sockfd);
										return 0;
									}
									
									memset(requete,0,(size_t) LONGUEUR_CODE);
									snprintf(requete,(size_t) LONGUEUR_CODE,"%d",(int)statbuf.st_size);
									
									if(send(serveur->sockfd,requete,(size_t) LONGUEUR_CODE, 0) < 0)
									{
										close(serveur->sockfd);
										return 0;
									}
									
									memset(reponse,0,(size_t) LONGUEUR_CODE);
									
									if(recv(serveur->sockfd, reponse, LONGUEUR_CODE, 0) <= 0)
									{
										close(serveur->sockfd);
										return 0;
									}
									if(strncmp(OKI,reponse, strlen(OKI)))
									{
										close(serveur->sockfd);
										return 0;
									}
									
									if(!statbuf.st_size)
									{
										close(serveur->sockfd);
										return 1;
									}
									
									if((fd = open("./fichiers_partages/description_fichiers.txt", O_RDONLY)) == -1)
									{
										close(serveur->sockfd);
										return 0;
									}
									
									if(sendfile(serveur->sockfd,fd,NULL, (size_t)statbuf.st_size) < 0)
									{
										close(serveur->sockfd);
										return 0;
									}
									
									close(serveur->sockfd);										
									return 1;
								}	
								else
								{
									close(serveur->sockfd);
									return 0;
								}	
								
							}	
						
						}	
						else
						{
							close(serveur->sockfd);
					    	return 0;
	
						}

					}

				}
				else
				{
					close(serveur->sockfd);
					return 0;
				}

			}
			
		//}

	//}	

}
