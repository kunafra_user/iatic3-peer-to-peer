
#ifndef CLIENT
#define CLIENT


#include <ncurses.h>
#include <signal.h>
#include "serveur.h"


#define NOM_UTILISATEUR_REGEX "^[a-zA-Z]+$"
#define ADDRESSE_IP_REGEX "^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
#define PORT_REGEX  "^([1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$"

#define FILE_LOOKUP "FILE_LOOKUP"


/*
 * Structure contenant les informations de connection.
 * 
 * 
 * ip_serv_central   : adresse IP du serveur centralisé.
 * port_serv_central : port d'écoute du serveur centralisé ( 50000 par défaut).
 * 
 * 
 * ip_serveur_distant: adresse IP du serveur de l'hôte sur lequel on va télécharger le fichier recherché.
 * port_serveur_distant: port d'écoute du serveur de l'hôte sur lequel on va télécharger le fichier recherché.
 * 
 * port_client       : port client.
 * socket_client     : descripteur de fichiers de type  socket du client.
 * 
 * 
 * status_connection : informe l'utilisateur sur l'état de la connection.
 * code_requete      : correspond au code de la requête de l'utilisateur (SIGN_UP,LOG_IN).
 */
 
typedef struct
{

	char ip_serv_central[BUFFER_SIZE];
	uint16_t port_serv_central;

	char ip_serveur_distant[BUFFER_SIZE];
	uint16_t port_serveur_distant;

	uint16_t port_client;
	int socket_client;

	char status_connection[BUFFER_SIZE];
	char code_requete[LONGUEUR_CODE];		

}CONNECTION_INFO;


/* Enumération des différentes interfaces
 * 
 * etat_affichage indique donc , l'interface dans laquelle l'utilisateur se trouve.
 * 
 * Il peut prendre les valeurs suivantes : 
 *                                         - MENU_PRINCIPAL : indique que l'utilisateur se trouve dans le menu principal.
 *                                         - CONNEXION_SERVEUR_CENTRALISE : indique que l'utilisateur se trouve dans l'interface de connexion au serveur centralisé 
 *                                         - etc ...
 */
  
typedef enum
{
	MENU_PRINCIPAL,CONNEXION_SERVEUR_CENTRALISE,ENREG_SERVEUR_CENTRALISE,AIDE1,AIDE2,MENU_RECHERCHE

}etat_affichage;

/*
 * Structure contenant les informations sur l'interface textuelle.
 * 
 * 
 * win                                        : correspond à la fenêtre dans laquelle est affichée les instructions.
 * form                                       : correspond au formulaire dans lequel l'utilisateur entre ses requêtes.
 * 
 * 
 * largeur_fenetre , hauteur_fenetre          : correspondent aux dimensions de la fenêtre win.
 * largeur_formulaire , hauteur_formulaire    : correspondent aux dimensions de la boîte de formulaire.
 * 
 * 
 * afficher_menu_etendu                       : booléen permettant de débloquer les différentes interfaces lorsque l'utilisateur a été identifié avec succès.
 * afficher                                   : correspond à l'interface dans laquelle on se trouve (voir etat_affichage).
 */

typedef struct
{
	WINDOW *win;
	WINDOW *form;
	int largeur_fenetre,hauteur_fenetre;
	int largeur_formulaire,hauteur_formulaire;
	int afficher_menu_etendu;
	etat_affichage afficher;

}MENU;


//Variable de controle indiquant si la fenêtre a été redimensionné.
int redim_fenetre = 0;

//Coordonnées du texte.
int text_y = 10;
int text_x = 0;


MENU menu;

//Fonctions d'initialisations.
void init_paires_couleurs();
void init_palette_couleurs();
void init_ncurses();
void init_dimensions_surface_fenetres();
void charge_cadre_fenetres();
void init_surface_fenetres();

//Charge le menu adéquat à afficher.
void charger_menu();

//Fonction de gestion du signal.
void signal_redimensionner_fenetre(int signal);

//Fonction permettant le redimensionnement de la fenêtre après avoir reçu un signal.
void redimensionner_fenetre();

//Fonctions d'affichage.
void afficher_entete(WINDOW *win,int largeur_fenetre);
void affiche_texte_formulaire(WINDOW *form);
void afficher_aide1();
void afficher_aide2();
void afficher_menu_enregistrement();
void afficher_menu_connexion();
void afficher_menu_recherche();
void afficher_menu_principal();

//Fonctions de gestion de la saisie de l'utilisateur.
int est_saisie_valide(WINDOW *form , int active_choix_etendu);
int  est_nom_utilisateur_valide(char *nom , char *buffer_erreur);
int  est_addresse_valide(char *adresseIP , char *buffer_erreur);
int  est_numero_port_valide(char *port , char *buffer_erreur);
void traite_requete_utilisateur(const char *requete_utilisateur);
void traite_choix_utilisateur(int choix_utilisateur, int active_choix_etendu);


//Fonction de rafraîchissement de la fenêtre.
void nettoyer_fenetre(WINDOW *win , WINDOW *form, int largeur_fenetre, int hauteur_formulaire);

#endif
